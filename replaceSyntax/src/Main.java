import java.io.*;
import java.util.regex.*;

public class Main {

    static int tempStartIndex;
    static int tempEndIndex;

    public static void main(String[] args) {
        // input the file content to the String "input"
        //String rootPath = "/Users/xufei/Desktop/Acumens/Acumen_Post_Syntax/acumen-dev-postsyntax/examples/";
        //String cpsIntro = "00_Demos/";
        //String filename = "07_Rice_wrist.acm";
        //String fileFullPath = rootPath + cpsIntro + filename;
        String fileFullPath = "/Users/xufei/Desktop/TempToChange.txt";
        //String fileFullPath = "/Users/xufei/Desktop/Acumens/Acumen_Post_Syntax/acumen-dev-postsyntax//src/test/resources/acumen/data/ShouldRun";
        HandleFile(fileFullPath);
    }

    public static File[] finder(String dirName){
        File dir = new File(dirName);

        return dir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String filename)
            { return filename.endsWith(".acm"); }
        } );

    }

    public static void HandleFile (String fileFullPath) {
        try {
            //File[] file = finder(fileFullPath);
            File file = new File(fileFullPath);

            BufferedReader reader = new BufferedReader(new FileReader(file));

            int words;
            String input = "";
            char ch;
            String tempChange;
            String newText;
            int countLeftBracket;
            int countRightBracket;
            int tempChar;
            String beforeReplace;
            String afterReplace;

            boolean flag3D = false;

            while ((words = reader.read()) != -1) {
                ch = (char) words;
                input += ch;
            }
            System.out.println(input);

            /*for (int i=0; i<file.length; i++) {
                BufferedReader reader = new BufferedReader(new FileReader(file[i]));

                int words;
                String input = "";
                char ch;
                String tempChange;
                String newText;
                int countLeftBracket;
                int countRightBracket;
                int tempChar;
                String beforeReplace;
                String afterReplace;

                boolean flag3D = false;

                while ((words = reader.read()) != -1) {
                    ch = (char) words;
                    input += ch;
                }
                System.out.println(input);

                newText = input.replaceAll("\\+ ==","+ =");

                tempStartIndex = 0;
                tempEndIndex = 0;
                while (newText.indexOf("==", tempStartIndex) != -1) {
                    tempStartIndex = newText.indexOf("==", tempStartIndex);
                    tempEndIndex = newText.indexOf("\n", tempStartIndex);
                    tempChange = newText.substring(tempStartIndex, tempEndIndex);
                    if (!tempChange.contains("then")) {
                        newText = new StringBuilder(newText).insert(tempStartIndex, "@").toString();
                        newText = newText.replaceAll("@==", "=");
                    }
                    tempStartIndex  = tempEndIndex;
                }

                System.out.println(newText);
                FileWriter writer = new FileWriter(file[i]);
                writer.write(newText);
                writer.close();
            }*/


            // replace the class Main => model Main
            newText = input.replaceAll("class Main\\(simulator\\)", "model Main(simulator) = ");
            // replace the private => initially
            newText = newText.replaceAll("private", "initially");
            newText = newText.replaceAll(";", " ,");
            // replace := => = in initially state
            tempStartIndex = 0;
            while ((newText.indexOf("initially", tempStartIndex)) != -1) {
                tempStartIndex = newText.indexOf("initially", tempStartIndex);
                tempEndIndex = newText.indexOf("end", tempStartIndex);
                if (tempEndIndex != -1) {
                    beforeReplace = newText.substring(tempStartIndex, tempEndIndex);
                    afterReplace = newText.substring(tempStartIndex, tempEndIndex).replaceAll(":=", " = ");
                    beforeReplace = Pattern.quote(beforeReplace);
                    newText = newText.replaceAll(beforeReplace, afterReplace);
                    tempStartIndex = tempEndIndex;
                } else {
                    break;
                }
            }
            // replace the 3D definition
            tempStartIndex = 0;
            tempEndIndex = 0;
            while (!flag3D){
                tempStartIndex = newText.indexOf("_3D", tempStartIndex);
                if (tempStartIndex != -1) {
                    if ((newText.indexOf("[", tempStartIndex)) != -1) {
                        tempStartIndex = newText.indexOf("[", tempStartIndex);
                    } else {
                        tempStartIndex = newText.indexOf("(", tempStartIndex);
                    }
                    // find the 3D code part
                    countLeftBracket = 0;
                    countRightBracket = 0;
                    for (int i = tempStartIndex; i > -1; i++){
                        tempChar = newText.charAt(i);
                        if (tempChar == '[' || tempChar == '(') {
                            countLeftBracket++ ;
                        } else if (tempChar == ']' || tempChar == ')') {
                            countRightBracket++ ;
                        }
                        if (countLeftBracket == countRightBracket) {
                            tempEndIndex = i;
                            break;
                        }
                    }
                    tempEndIndex++ ;
                    newText = replaceDefine(newText, tempStartIndex, tempEndIndex, "\"Text\"", "Text");
                    newText = replaceDefine(newText, tempStartIndex, tempEndIndex, "\"Box\"", "Box");
                    newText = replaceDefine(newText, tempStartIndex, tempEndIndex, "\"Cylinder\"", "Cylinder");
                    newText = replaceDefine(newText, tempStartIndex, tempEndIndex, "\"Sphere\"", "Sphere");
                    newText = replaceDefine(newText, tempStartIndex, tempEndIndex, "\"Cone\"", "Cone");
                    newText = replaceDefine(newText, tempStartIndex, tempEndIndex, "\"Obj\"", "Obj");
                    tempChange = newText.substring(tempStartIndex, tempEndIndex);
                    //tempChange = Pattern.quote(tempChange);
                    tempChange = tempChange.replaceAll("\\[", "(");
                    tempChange = tempChange.replaceAll("\\]", ")");
                    // change all text 3D
                    tempChange = replaceText(tempChange);
                    tempChange = replaceBox(tempChange);
                    tempChange = replaceCylinder(tempChange);
                    tempChange = replaceCone(tempChange);
                    tempChange = replaceSphere(tempChange);
                    tempChange = replaceObj(tempChange);
                    String pattern3D = Pattern.quote(newText.substring(tempStartIndex, tempEndIndex));
                    newText = newText.replaceAll(pattern3D, tempChange);
                } else {
                    flag3D = true;
                }
            }
            // replace end of initially to always
            newText = newText.replaceAll("class", "model");
            tempStartIndex = 0;
            while((newText.indexOf("model", tempStartIndex)) != -1) {
                tempStartIndex = newText.indexOf("model", tempStartIndex);
                tempEndIndex = newText.indexOf("end", tempStartIndex);
                if (tempEndIndex != -1) {
                    newText = new StringBuilder(newText).insert(tempEndIndex, "@").toString();
                    tempStartIndex = tempEndIndex;
                } else {
                    tempStartIndex++ ;
                }

                newText = newText.replaceAll("@end", "always");
            }
            // replace := and ; between always and the end of model
            tempStartIndex = 0;
            while((newText.indexOf("always", tempStartIndex)) != -1) {
                tempStartIndex = newText.indexOf("always", tempStartIndex);
                tempEndIndex = newText.indexOf("model", tempStartIndex);
                // there's no model anymore
                if (tempEndIndex == -1) {
                    tempEndIndex = newText.lastIndexOf("end");
                }
                if (tempEndIndex == -1 || tempEndIndex < tempStartIndex) {
                    tempStartIndex++ ;
                } else {
                    beforeReplace = newText.substring(tempStartIndex, tempEndIndex);
                    afterReplace = newText.substring(tempStartIndex, tempEndIndex).replaceAll(":=","+ =");
                    beforeReplace = Pattern.quote(beforeReplace);
                    newText = newText.replaceAll(beforeReplace, afterReplace);
                    beforeReplace = newText.substring(tempStartIndex, tempEndIndex);
                    afterReplace = newText.substring(tempStartIndex, tempEndIndex).replaceAll(";"," ,");
                    beforeReplace = Pattern.quote(beforeReplace);
                    newText = newText.replaceAll(beforeReplace, afterReplace);
                    tempStartIndex = tempEndIndex;
                }
            }
            // delete the last end
            tempStartIndex = newText.lastIndexOf("end");
            if (tempStartIndex != -1) {
                newText = new StringBuilder(newText).insert(tempStartIndex, "@").toString();
                newText = newText.replaceAll("@end", " ");
            }

            // Double check the = after model XXX
            tempStartIndex = 0;
            while ((newText.indexOf("model", tempStartIndex)) != -1) {
                tempStartIndex = newText.indexOf("model", tempStartIndex);
                tempEndIndex = newText.indexOf("initially", tempStartIndex);
                if (tempEndIndex != -1) {
                    String doubleCheck = newText.substring(tempStartIndex, tempEndIndex);
                    if (!doubleCheck.contains("=")) {
                        newText = new StringBuffer(newText).insert(tempEndIndex, "= ").toString();
                    }
                    tempStartIndex = tempEndIndex;
                } else {
                    tempStartIndex++;
                }
            }

            // change all the brackets into ()
            newText = newText.replaceAll("\\[", "(");
            newText = newText.replaceAll("\\]", ")");

            // change switch case to match case with [

            tempStartIndex = 0;
            newText = newText.replaceAll("switch mode", "match mode with [");
            while ((newText.indexOf("case \"", tempStartIndex)) != -1) {
                tempStartIndex = newText.indexOf("case \"", tempStartIndex);
                tempStartIndex = newText.indexOf("\"", tempStartIndex+6);
                newText = new StringBuilder(newText).insert(tempStartIndex+2, "->").toString();
            }

            newText = newText.replaceAll("case", " ");

            System.out.println(newText);
            FileWriter writer = new FileWriter(fileFullPath);
            writer.write(newText);
            writer.close();

        } catch (Exception e) {
            System.out.println("Problem reading file.");
            e.printStackTrace();
        }
    }

    public static String replaceText (String change) {
        int tempComma = 0;
        int countComma;
        String tempChange = change;
        while (tempChange.indexOf("Text", tempComma) != -1) {
            // find center vector
            tempComma = tempChange.indexOf("Text", tempComma);
            tempComma = tempChange.indexOf(",", tempComma);
            tempChange = new StringBuilder(tempChange).insert(tempComma, "@").toString();
            tempChange = tempChange.replaceAll("@," , " center=");
            // find size
            tempComma = tempChange.indexOf("," , tempComma);
            countComma = 0;
            for (int i = tempComma; i > -1; i++) {
                if (tempChange.charAt(i) == ',') {
                    countComma++ ;
                }
                if (countComma == 3) {
                    tempComma = i;
                    break;
                }
            }
            tempChange = new StringBuilder(tempChange).insert(tempComma, "@").toString();
            tempChange = tempChange.replaceAll("@," , " size=");
            // find color
            tempComma = tempChange.indexOf("," , tempComma);
            tempChange = new StringBuilder(tempChange).insert(tempComma, "@").toString();
            tempChange = tempChange.replaceAll("@," , " color=");
            // find rotation
            if (tempChange.indexOf(",", tempComma) > tempChange.indexOf("(", tempComma)) {
                // use vector as color
                tempComma = tempChange.indexOf("," , tempComma);
                countComma = 0;
                for (int i = tempComma; i > -1; i++) {
                    if (tempChange.charAt(i) == ',') {
                        countComma++ ;
                    }
                    if (countComma == 3) {
                        tempComma = i;
                        break;
                    }
                }
                tempChange = new StringBuilder(tempChange).insert(tempComma, "@").toString();
                tempChange = tempChange.replaceAll("@," , " rotation=");
            } else {
                tempComma = tempChange.indexOf("," , tempComma);
                tempChange = new StringBuilder(tempChange).insert(tempComma, "@").toString();
                tempChange = tempChange.replaceAll("@," , " rotation=");
            }
            // find content
            tempComma = tempChange.indexOf("," , tempComma);
            countComma = 0;
            for (int i = tempComma; i > -1; i++) {
                if (tempChange.charAt(i) == ',') {
                    countComma++ ;
                }
                if (countComma == 3) {
                    tempComma = i;
                    break;
                }
            }
            tempChange = new StringBuilder(tempChange).insert(tempComma, "@").toString();
            tempChange = tempChange.replaceAll("@," , " content=");
        }
        return tempChange;
    }

    public static String replaceBox (String change) {
        int tempComma = 0;
        int countComma;
        String tempChange = change;
        while (tempChange.indexOf("Box", tempComma) != -1) {
            // find center vector
            tempComma = tempChange.indexOf("Box", tempComma);
            tempComma = tempChange.indexOf(",", tempComma);
            tempChange = new StringBuilder(tempChange).insert(tempComma, "@").toString();
            tempChange = tempChange.replaceAll("@," , " center=");
            // find size
            tempComma = tempChange.indexOf("," , tempComma);
            countComma = 0;
            for (int i = tempComma; i > -1; i++) {
                if (tempChange.charAt(i) == ',') {
                    countComma++ ;
                }
                if (countComma == 3) {
                    tempComma = i;
                    break;
                }
            }
            tempChange = new StringBuilder(tempChange).insert(tempComma, "@").toString();
            tempChange = tempChange.replaceAll("@," , " size=");
            // find color
            tempComma = tempChange.indexOf("," , tempComma);
            countComma = 0;
            for (int i = tempComma; i > -1; i++) {
                if (tempChange.charAt(i) == ',') {
                    countComma++ ;
                }
                if (countComma == 3) {
                    tempComma = i;
                    break;
                }
            }
            tempChange = new StringBuilder(tempChange).insert(tempComma, "@").toString();
            tempChange = tempChange.replaceAll("@," , " color=");
            // find rotation
            if (tempChange.indexOf(",", tempComma) > tempChange.indexOf("(", tempComma)) {
                // use vector as color
                tempComma = tempChange.indexOf("," , tempComma);
                countComma = 0;
                for (int i = tempComma; i > -1; i++) {
                    if (tempChange.charAt(i) == ',') {
                        countComma++ ;
                    }
                    if (countComma == 3) {
                        tempComma = i;
                        break;
                    }
                }
                tempChange = new StringBuilder(tempChange).insert(tempComma, "@").toString();
                tempChange = tempChange.replaceAll("@," , " rotation=");
            } else {
                tempComma = tempChange.indexOf("," , tempComma);
                tempChange = new StringBuilder(tempChange).insert(tempComma, "@").toString();
                tempChange = tempChange.replaceAll("@," , " rotation=");
            }
        }
        return tempChange;
    }

    public static String replaceCylinder (String change) {
        int tempComma = 0;
        int countComma;
        String tempChange = change;
        int tempStart;
        int tempEnd;
        int tempLeftBracket;
        int tempRightBracket;
        while (tempChange.indexOf("Cylinder", tempComma) != -1) {
            // find center vector
            tempComma = tempChange.indexOf("Cylinder", tempComma);
            tempStart = tempComma;
            tempComma = tempChange.indexOf(",", tempComma);
            tempChange = new StringBuilder(tempChange).insert(tempComma, "@").toString();
            tempChange = tempChange.replaceAll("@," , " center=");
            // find radius
            tempComma = tempChange.indexOf("," , tempComma);
            countComma = 0;
            for (int i = tempComma; i > -1; i++) {
                if (tempChange.charAt(i) == ',') {
                    countComma++ ;
                }
                if (countComma == 3) {
                    tempComma = i;
                    break;
                }
            }
            tempChange = new StringBuilder(tempChange).insert(tempComma, "@").toString();
            tempChange = tempChange.replaceAll("@," , " radius=");
            // find length
            tempComma = tempChange.indexOf("," , tempComma);
            tempChange = new StringBuilder(tempChange).insert(tempComma, "@").toString();
            tempChange = tempChange.replaceAll("@," , " length=");
            // find color
            tempComma = tempChange.indexOf("," , tempComma);
            tempChange = new StringBuilder(tempChange).insert(tempComma, "@").toString();
            tempChange = tempChange.replaceAll("@," , " color=");
            // find rotation
            if (tempChange.indexOf(",", tempComma) > tempChange.indexOf("(", tempComma)) {
                // use vector as color
                tempComma = tempChange.indexOf("," , tempComma);
                countComma = 0;
                for (int i = tempComma; i > -1; i++) {
                    if (tempChange.charAt(i) == ',') {
                        countComma++ ;
                    }
                    if (countComma == 3) {
                        tempComma = i;
                        break;
                    }
                }
                tempChange = new StringBuilder(tempChange).insert(tempComma, "@").toString();
                tempChange = tempChange.replaceAll("@," , " rotation=");
            } else {
                tempComma = tempChange.indexOf("," , tempComma);
                tempChange = new StringBuilder(tempChange).insert(tempComma, "@").toString();
                tempChange = tempChange.replaceAll("@," , " rotation=");
            }
            tempStart = tempChange.indexOf("radius", tempStart);
            tempEnd = tempChange.indexOf("color", tempStart);
            // delete left bracket
            tempLeftBracket = tempChange.indexOf("[", tempStart);
            if (tempLeftBracket == -1) {
                tempLeftBracket = tempChange.indexOf("(", tempStart);
            }
            if (tempLeftBracket != -1) {
                tempChange = new StringBuffer(tempChange).insert(tempLeftBracket, "@").toString();
                tempChange = tempChange.replaceAll("@\\(" , " ");
                tempChange = tempChange.replaceAll("@\\[" , " ");
            }
            // delete right bracket
            tempRightBracket = tempChange.lastIndexOf("]", tempEnd);
            if (tempRightBracket == -1) {
                tempRightBracket = tempChange.lastIndexOf(")", tempEnd);
            }
            if (tempRightBracket != -1) {
                tempChange = new StringBuffer(tempChange).insert(tempRightBracket, "@").toString();
                tempChange = tempChange.replaceAll("@\\)" , " ");
                tempChange = tempChange.replaceAll("@\\]" , " ");
            }
        }
        return tempChange;
    }

    public static String replaceCone (String change) {
        int tempComma = 0;
        int countComma;
        String tempChange = change;
        int tempStart;
        int tempEnd;
        int tempLeftBracket;
        int tempRightBracket;
        while (tempChange.indexOf("Cone", tempComma) != -1) {
            // find center vector
            tempComma = tempChange.indexOf("Cone", tempComma);
            tempStart = tempComma;
            tempComma = tempChange.indexOf(",", tempComma);
            tempChange = new StringBuilder(tempChange).insert(tempComma, "@").toString();
            tempChange = tempChange.replaceAll("@," , " center=");
            // find radius
            tempComma = tempChange.indexOf("," , tempComma);
            countComma = 0;
            for (int i = tempComma; i > -1; i++) {
                if (tempChange.charAt(i) == ',') {
                    countComma++ ;
                }
                if (countComma == 3) {
                    tempComma = i;
                    break;
                }
            }
            tempChange = new StringBuilder(tempChange).insert(tempComma, "@").toString();
            tempChange = tempChange.replaceAll("@," , " radius=");
            // find length
            tempComma = tempChange.indexOf("," , tempComma);
            tempChange = new StringBuilder(tempChange).insert(tempComma, "@").toString();
            tempChange = tempChange.replaceAll("@," , " length=");
            // find color
            tempComma = tempChange.indexOf("," , tempComma);
            tempChange = new StringBuilder(tempChange).insert(tempComma, "@").toString();
            tempChange = tempChange.replaceAll("@," , " color=");
            // find rotation
            if (tempChange.indexOf(",", tempComma) > tempChange.indexOf("(", tempComma)) {
                // use vector as color
                tempComma = tempChange.indexOf("," , tempComma);
                countComma = 0;
                for (int i = tempComma; i > -1; i++) {
                    if (tempChange.charAt(i) == ',') {
                        countComma++ ;
                    }
                    if (countComma == 3) {
                        tempComma = i;
                        break;
                    }
                }
                tempChange = new StringBuilder(tempChange).insert(tempComma, "@").toString();
                tempChange = tempChange.replaceAll("@," , " rotation=");
            } else {
                tempComma = tempChange.indexOf("," , tempComma);
                tempChange = new StringBuilder(tempChange).insert(tempComma, "@").toString();
                tempChange = tempChange.replaceAll("@," , " rotation=");
            }
            tempStart = tempChange.indexOf("radius", tempStart);
            tempEnd = tempChange.indexOf("color", tempStart);
            // delete left bracket
            tempLeftBracket = tempChange.indexOf("[", tempStart);
            if (tempLeftBracket == -1) {
                tempLeftBracket = tempChange.indexOf("(", tempStart);
            }
            if (tempLeftBracket != -1) {
                tempChange = new StringBuffer(tempChange).insert(tempLeftBracket, "@").toString();
                tempChange = tempChange.replaceAll("@\\(" , " ");
                tempChange = tempChange.replaceAll("@\\[" , " ");
            }
            // delete right bracket
            tempRightBracket = tempChange.lastIndexOf("]", tempEnd);
            if (tempRightBracket == -1) {
                tempRightBracket = tempChange.lastIndexOf(")", tempEnd);
            }
            if (tempRightBracket != -1) {
                tempChange = new StringBuffer(tempChange).insert(tempRightBracket, "@").toString();
                tempChange = tempChange.replaceAll("@\\)" , " ");
                tempChange = tempChange.replaceAll("@\\]" , " ");
            }
        }
        return tempChange;
    }

    public static String replaceSphere (String change) {
        int tempComma = 0;
        int countComma;
        String tempChange = change;
        while (tempChange.indexOf("Sphere", tempComma) != -1) {
            // find center vector
            tempComma = tempChange.indexOf("Sphere", tempComma);
            tempComma = tempChange.indexOf(",", tempComma);
            tempChange = new StringBuilder(tempChange).insert(tempComma, "@").toString();
            tempChange = tempChange.replaceAll("@," , " center=");
            // find size
            tempComma = tempChange.indexOf("," , tempComma);
            countComma = 0;
            for (int i = tempComma; i > -1; i++) {
                if (tempChange.charAt(i) == ',') {
                    countComma++ ;
                }
                if (countComma == 3) {
                    tempComma = i;
                    break;
                }
            }
            tempChange = new StringBuilder(tempChange).insert(tempComma, "@").toString();
            tempChange = tempChange.replaceAll("@," , " size=");
            // find color
            tempComma = tempChange.indexOf("," , tempComma);
            tempChange = new StringBuilder(tempChange).insert(tempComma, "@").toString();
            tempChange = tempChange.replaceAll("@," , " color=");
            // find rotation
            if (tempChange.indexOf(",", tempComma) > tempChange.indexOf("(", tempComma)) {
                // use vector as color
                tempComma = tempChange.indexOf("," , tempComma);
                countComma = 0;
                for (int i = tempComma; i > -1; i++) {
                    if (tempChange.charAt(i) == ',') {
                        countComma++ ;
                    }
                    if (countComma == 3) {
                        tempComma = i;
                        break;
                    }
                }
                tempChange = new StringBuilder(tempChange).insert(tempComma, "@").toString();
                tempChange = tempChange.replaceAll("@," , " rotation=");
            } else {
                tempComma = tempChange.indexOf("," , tempComma);
                tempChange = new StringBuilder(tempChange).insert(tempComma, "@").toString();
                tempChange = tempChange.replaceAll("@," , " rotation=");
            }
        }
        return tempChange;
    }

    public static String replaceObj (String change) {
        int tempComma = 0;
        int countComma;
        String tempChange = change;
        while (tempChange.indexOf("Obj", tempComma) != -1) {
            // find center vector
            tempComma = tempChange.indexOf("Obj", tempComma);
            tempComma = tempChange.indexOf(",", tempComma);
            tempChange = new StringBuilder(tempChange).insert(tempComma, "@").toString();
            tempChange = tempChange.replaceAll("@," , " center=");
            // find size
            tempComma = tempChange.indexOf("," , tempComma);
            countComma = 0;
            for (int i = tempComma; i > -1; i++) {
                if (tempChange.charAt(i) == ',') {
                    countComma++ ;
                }
                if (countComma == 3) {
                    tempComma = i;
                    break;
                }
            }
            tempChange = new StringBuilder(tempChange).insert(tempComma, "@").toString();
            tempChange = tempChange.replaceAll("@," , " size=");
            // find color
            tempComma = tempChange.indexOf("," , tempComma);
            tempChange = new StringBuilder(tempChange).insert(tempComma, "@").toString();
            tempChange = tempChange.replaceAll("@," , " color=");
            // find rotation
            if (tempChange.indexOf(",", tempComma) > tempChange.indexOf("(", tempComma)) {
                // use vector as color
                tempComma = tempChange.indexOf("," , tempComma);
                countComma = 0;
                for (int i = tempComma; i > -1; i++) {
                    if (tempChange.charAt(i) == ',') {
                        countComma++ ;
                    }
                    if (countComma == 3) {
                        tempComma = i;
                        break;
                    }
                }
                tempChange = new StringBuilder(tempChange).insert(tempComma, "@").toString();
                tempChange = tempChange.replaceAll("@," , " rotation=");
            } else {
                tempComma = tempChange.indexOf("," , tempComma);
                tempChange = new StringBuilder(tempChange).insert(tempComma, "@").toString();
                tempChange = tempChange.replaceAll("@," , " rotation=");
            }
        }
        return tempChange;
    }

    public static String replaceDefine (String newText, int tempStartIndex, int tempEndIndex, String textBefore, String textAfter) {

        String beforeReplace;
        String afterReplace;
        String tempNewText = newText;

        beforeReplace = tempNewText.substring(tempStartIndex, tempEndIndex);
        String pattern = Pattern.quote(beforeReplace);
        afterReplace = tempNewText.substring(tempStartIndex, tempEndIndex).replaceAll(textBefore, textAfter);
        tempNewText = tempNewText.replaceAll(pattern, afterReplace);
        return tempNewText;
    }
}
